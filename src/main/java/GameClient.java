import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameClient {

	public static void main(String[] args) throws IOException {
		Logger logger = LoggerFactory.getLogger(GameClient.class);
		int roll = 0;
		Game g = new Game();
		
		for(int i = 0; i < 10; i++) {
			roll = newRandomRoll();
			g.roll(roll);
			
			logger.info("Roll: {} score: {}", roll, g.score());
		}
	}
	
	public static int newRandomRoll() {
		return (int) Math.ceil(Math.random() * 10);
	}
}
