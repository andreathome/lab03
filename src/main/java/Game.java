
public class Game {
	private final short ROLL1STATE = 0, ROLL2STATE = 1, SPARESTATE = 2, STRIKESTATE = 3;
	
	private short state;
	private int prevRoll, frameNum, points;
	private boolean addStrikeScore;
	
	public Game() {
		this.state = 0;
		this.prevRoll = 0;
		this.frameNum = 0;
		this.points= 0;
		this.addStrikeScore = false;
	}
	
	public void roll(int pins) {
		switch(state) {
			case ROLL1STATE:
				roll1State(pins);
				break;
			case ROLL2STATE:
				roll2State(pins);
				break;
			case SPARESTATE:
				spareState(pins);
				break;
			case STRIKESTATE:
				strikeState(pins);
				break;		
			// invalid state
			default:
				System.err.println("Invalid game state");
				break;
		}
		
		prevRoll = pins;
	}

	public int score() {
		return points;
	}
	
	private void roll1State(int pins) {
		if(pins == 10)
			frameNum++;
		
		points += pins;
		
		state = pins < 10 ? ROLL2STATE : STRIKESTATE;
	}
	
	private void roll2State(int pins) {
		frameNum++;
		
		if(addStrikeScore) {
			points += 2 * pins;
			addStrikeScore = false;
		}
		else	
			points += pins;
		
		state = prevRoll + pins < 10 ? ROLL1STATE : SPARESTATE;
	}

	private void spareState(int pins) {
		if(pins == 10)
			frameNum++;
		
		if(frameNum < 11)
			points += 2 * pins;
		else
			points += pins;
		
		state = pins < 10 ? ROLL2STATE : STRIKESTATE;
	}
	
	private void strikeState(int pins) {
		if(pins == 10)
			frameNum++;
		
		// cumulate strike prize until last frame
		if(addStrikeScore && frameNum < 10)
			points += pins;
		
		points += 2 * pins;
		addStrikeScore = true;
		
		state = pins < 10 ? ROLL2STATE : STRIKESTATE;
	}
}
